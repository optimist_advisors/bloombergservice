﻿using System;
using System.Collections.Generic;
using Bloomberglp.Blpapi;
using Moq;
using NUnit.Framework;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomber.Service.Test
{
    public class SessionManagerTest
    {
        [Test]
        public void TestOpen()
        {
            var mockSessionFactory = new Mock<ISessionFactory>(MockBehavior.Strict);
            var sessionManager = new SessionFactory();
            var mockSession = new Mock<Session>(MockBehavior.Strict);
            mockSessionFactory.Setup(m => m.Create()).Returns(new Session());
            var ex = Assert.Throws<Exception>(() => sessionManager.RunOnSession(session => { }));



        }
    }

    public class BloombergServiceTest
    {
        private Mock<ISessionFactory> mockSessionFactory;
        private Mock<IRequestFactory> mockRequestFactory;
        private BloombergService service;
        private List<Action<Session>> pendingActions;
        private Mock<Session> mockSession;
        private Mock<Bloomberglp.Blpapi.Service> mockBbgService;

        [SetUp]
        public void Setup()
        {
            mockSessionFactory =new Mock<ISessionFactory>(MockBehavior.Strict);
            mockRequestFactory = new Mock<IRequestFactory>(MockBehavior.Strict);
            service = new BloombergService(mockRequestFactory.Object,mockSessionFactory.Object);
            pendingActions = new List<Action<Session>>();
            mockSession = new Mock<Session>();
            mockSessionFactory.Setup(m => m.RunOnSession(It.IsAny<Action<Session>>()));
            mockBbgService = new Mock<Bloomberglp.Blpapi.Service>(MockBehavior.Strict);

        }

        [Test]
        public void TestHistorical()
        {
            var from  = DateTime.Today.AddDays(-1);
            var to = DateTime.Today;
            var result = service.GetHistorical(new List<string>() {"AD1 Curncy"}, from, to);
        }

        [Test]
        public void TestReference()
        {
            //todo: Cant mock BBG Session, find a way for mocking it so we can test the service
            var result = service.GetReferenceData(new List<string>() { "AD1 Curncy" }, new List<string>() {"FUT_CUR_GEN_TICKER"},new List<FieldValueDto>());
        }
    }
}
