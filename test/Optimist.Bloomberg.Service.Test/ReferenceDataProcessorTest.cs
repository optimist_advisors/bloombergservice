using System;
using Bloomberglp.Blpapi;
using Moq;
using NUnit.Framework;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Service.Reference;

namespace Optimist.Bloomber.Service.Test
{
    public class ReferenceDataProcessorTest
    {
        [Test]
        public void TestEmpty()
        {
            var processor = new ReferenceDataResponseProcessor();
            var mockmessage = new Mock<Message>(MockBehavior.Strict);
            var mockMessageElement = new Mock<Element>(MockBehavior.Strict);
            var mockElementDefinition = new Mock<SchemaElementDefinition>(MockBehavior.Loose);
            var mockSchemaTypeDefinition = new Mock<SchemaTypeDefinition>(MockBehavior.Loose);
            mockElementDefinition.SetupGet(m => m.TypeDefinition).Returns(mockSchemaTypeDefinition.Object);
            mockmessage.Setup(m => m.AsElement).Returns(mockMessageElement.Object);

            mockMessageElement.SetupGet(m => m.NumValues).Returns(0);
            mockMessageElement.SetupGet(m => m.NumElements).Returns(0);
            mockMessageElement.SetupGet(m => m.Name).Returns(BlpApiDictionary.HistoricalDataResponse);
            mockMessageElement.SetupGet(m => m.ElementDefinition).Returns(mockElementDefinition.Object);
            mockMessageElement.SetupGet(m => m.Datatype).Returns(Schema.Datatype.CHOICE);

            var securityElement = new Mock<Element>(MockBehavior.Loose);
            securityElement.Setup(m => m.GetElementAsString(BlpApiDictionary.Security)).Returns("AD1 Comdty");

            mockmessage.Setup(m => m.GetElement(BlpApiDictionary.SecurityData)).Returns(securityElement.Object);

            var fieldDataElement = new Mock<Element>(MockBehavior.Loose);
            securityElement.Setup(m => m.GetElement(BlpApiDictionary.FieldData)).Returns(fieldDataElement.Object);

            var px = processor.ProcessMessage(mockmessage.Object);
            Assert.AreEqual(0, px.Count);
        }


        [Test]
        public void OneValue()
        {
            var processor = new ReferenceDataResponseProcessor();
            var mockmessage = new Mock<Message>(MockBehavior.Strict);
            var mockMessageElement = new Mock<Element>(MockBehavior.Strict);
            var mockElementDefinition = new Mock<SchemaElementDefinition>(MockBehavior.Loose);
            var mockSchemaTypeDefinition = new Mock<SchemaTypeDefinition>(MockBehavior.Loose);
            mockElementDefinition.SetupGet(m => m.TypeDefinition).Returns(mockSchemaTypeDefinition.Object);
            mockmessage.Setup(m => m.AsElement).Returns(mockMessageElement.Object);

            mockMessageElement.SetupGet(m => m.NumValues).Returns(0);
            mockMessageElement.SetupGet(m => m.NumElements).Returns(0);
            mockMessageElement.SetupGet(m => m.Name).Returns(BlpApiDictionary.ReferenceDataResponse);
            mockMessageElement.SetupGet(m => m.ElementDefinition).Returns(mockElementDefinition.Object);
            mockMessageElement.SetupGet(m => m.Datatype).Returns(Schema.Datatype.CHOICE);

            var securitiesElement = new Mock<Element>(MockBehavior.Loose);
            mockmessage.Setup(m => m.GetElement(BlpApiDictionary.SecurityData)).Returns(securitiesElement.Object);

            securitiesElement.SetupGet(m => m.NumValues).Returns(1);
            var securityElement = new Mock<Element>(MockBehavior.Strict);
            securityElement.Setup(s => s.GetElementAsString(BlpApiDictionary.Security)).Returns("AD1 Comdty");
            securitiesElement.Setup(s => s.GetValueAsElement(0)).Returns(securityElement.Object);
            securityElement.Setup(m => m.HasElement(BlpApiDictionary.SecurityError)).Returns(false);
            securityElement.Setup(m => m.HasElement(BlpApiDictionary.FieldData)).Returns(true);
            var fieldsElement = new Mock<Element>(MockBehavior.Strict);
            securityElement.Setup(m => m.GetElement(BlpApiDictionary.FieldData)).Returns(fieldsElement.Object);
            fieldsElement.SetupGet(s => s.NumElements).Returns(1);

            var mockFieldElement = new Mock<Element>();
            fieldsElement.Setup(m => m.GetElement(0)).Returns(mockFieldElement.Object);
            mockFieldElement.Setup(m => m.GetValueAsString())
                .Returns("Value");
            mockFieldElement.SetupGet(m => m.Name).Returns(new Name("FLD_NAME"));

            var referenceDataResponseDtos = processor.ProcessMessage(mockmessage.Object);
            Assert.AreEqual(1, referenceDataResponseDtos.Count);
            Assert.AreEqual("AD1 Comdty", referenceDataResponseDtos[0].Ticker);
            Assert.AreEqual("FLD_NAME", referenceDataResponseDtos[0].Fields[0].Name);
            Assert.AreEqual("Value", referenceDataResponseDtos[0].Fields[0].Value);
            Assert.IsTrue(string.IsNullOrEmpty(referenceDataResponseDtos[0].Errors));
        }


        [Test]
        public void SeveralAssets()
        {
            var processor = new ReferenceDataResponseProcessor();
            var mockmessage = new Mock<Message>(MockBehavior.Strict);
            var mockMessageElement = new Mock<Element>(MockBehavior.Strict);
            var mockElementDefinition = new Mock<SchemaElementDefinition>(MockBehavior.Loose);
            var mockSchemaTypeDefinition = new Mock<SchemaTypeDefinition>(MockBehavior.Loose);
            mockElementDefinition.SetupGet(m => m.TypeDefinition).Returns(mockSchemaTypeDefinition.Object);
            mockmessage.Setup(m => m.AsElement).Returns(mockMessageElement.Object);

            mockMessageElement.SetupGet(m => m.NumValues).Returns(0);
            mockMessageElement.SetupGet(m => m.NumElements).Returns(0);
            mockMessageElement.SetupGet(m => m.Name).Returns(BlpApiDictionary.ReferenceDataResponse);
            mockMessageElement.SetupGet(m => m.ElementDefinition).Returns(mockElementDefinition.Object);
            mockMessageElement.SetupGet(m => m.Datatype).Returns(Schema.Datatype.CHOICE);

            var securitiesElement = new Mock<Element>(MockBehavior.Loose);
            mockmessage.Setup(m => m.GetElement(BlpApiDictionary.SecurityData)).Returns(securitiesElement.Object);

            securitiesElement.SetupGet(m => m.NumValues).Returns(2);
            var security1 = new Mock<Element>(MockBehavior.Strict);
            security1.Setup(s => s.GetElementAsString(BlpApiDictionary.Security)).Returns("AD1 Comdty");
            securitiesElement.Setup(s => s.GetValueAsElement(0)).Returns(security1.Object);
            security1.Setup(m => m.HasElement(BlpApiDictionary.SecurityError)).Returns(false);
            security1.Setup(m => m.HasElement(BlpApiDictionary.FieldData)).Returns(true);
            var fieldsElement = new Mock<Element>(MockBehavior.Strict);
            security1.Setup(m => m.GetElement(BlpApiDictionary.FieldData)).Returns(fieldsElement.Object);

            var security2 = new Mock<Element>(MockBehavior.Strict);
            security2.Setup(s => s.GetElementAsString(BlpApiDictionary.Security)).Returns("CT1 Comdty");
            security2.Setup(m => m.HasElement(BlpApiDictionary.SecurityError)).Returns(false);
            security2.Setup(m => m.HasElement(BlpApiDictionary.FieldData)).Returns(true);
            security2.Setup(m => m.GetElement(BlpApiDictionary.FieldData)).Returns(fieldsElement.Object);


            securitiesElement.Setup(s => s.GetValueAsElement(1)).Returns(security2.Object);

            fieldsElement.SetupGet(s => s.NumElements).Returns(1);

            var mockFieldElement = new Mock<Element>();
            fieldsElement.Setup(m => m.GetElement(0)).Returns(mockFieldElement.Object);
            mockFieldElement.Setup(m => m.GetValueAsString())
                .Returns("Value");
            mockFieldElement.SetupGet(m => m.Name).Returns(new Name("FLD_NAME"));

            var referenceDataResponseDtos = processor.ProcessMessage(mockmessage.Object);
            Assert.AreEqual(2, referenceDataResponseDtos.Count);
            Assert.AreEqual("AD1 Comdty", referenceDataResponseDtos[0].Ticker);
            Assert.AreEqual("FLD_NAME", referenceDataResponseDtos[0].Fields[0].Name);
            Assert.AreEqual("Value", referenceDataResponseDtos[0].Fields[0].Value);
            Assert.IsTrue(string.IsNullOrEmpty(referenceDataResponseDtos[0].Errors));
        }

        [Test]
        public void SeveralFields()
        {
            var processor = new ReferenceDataResponseProcessor();
            var mockmessage = new Mock<Message>(MockBehavior.Strict);
            var mockMessageElement = new Mock<Element>(MockBehavior.Strict);
            var mockElementDefinition = new Mock<SchemaElementDefinition>(MockBehavior.Loose);
            var mockSchemaTypeDefinition = new Mock<SchemaTypeDefinition>(MockBehavior.Loose);
            mockElementDefinition.SetupGet(m => m.TypeDefinition).Returns(mockSchemaTypeDefinition.Object);
            mockmessage.Setup(m => m.AsElement).Returns(mockMessageElement.Object);

            mockMessageElement.SetupGet(m => m.NumValues).Returns(0);
            mockMessageElement.SetupGet(m => m.NumElements).Returns(0);
            mockMessageElement.SetupGet(m => m.Name).Returns(BlpApiDictionary.ReferenceDataResponse);
            mockMessageElement.SetupGet(m => m.ElementDefinition).Returns(mockElementDefinition.Object);
            mockMessageElement.SetupGet(m => m.Datatype).Returns(Schema.Datatype.CHOICE);

            var securitiesElement = new Mock<Element>(MockBehavior.Loose);
            mockmessage.Setup(m => m.GetElement(BlpApiDictionary.SecurityData)).Returns(securitiesElement.Object);

            securitiesElement.SetupGet(m => m.NumValues).Returns(1);
            var securityElement = new Mock<Element>(MockBehavior.Strict);
            securityElement.Setup(s => s.GetElementAsString(BlpApiDictionary.Security)).Returns("AD1 Comdty");
            securitiesElement.Setup(s => s.GetValueAsElement(0)).Returns(securityElement.Object);
            securityElement.Setup(m => m.HasElement(BlpApiDictionary.SecurityError)).Returns(false);
            securityElement.Setup(m => m.HasElement(BlpApiDictionary.FieldData)).Returns(true);
            var fieldsElement = new Mock<Element>(MockBehavior.Strict);
            securityElement.Setup(m => m.GetElement(BlpApiDictionary.FieldData)).Returns(fieldsElement.Object);
            fieldsElement.SetupGet(s => s.NumElements).Returns(2);

            var field1 = new Mock<Element>();
            fieldsElement.Setup(m => m.GetElement(0)).Returns(field1.Object);
            field1.Setup(m => m.GetValueAsString())
                .Returns("Value");
            field1.SetupGet(m => m.Name).Returns(new Name("FLD1_NAME"));

            var field2 = new Mock<Element>();
            fieldsElement.Setup(m => m.GetElement(1)).Returns(field2.Object);
            field2.Setup(m => m.GetValueAsString())
                .Returns("Value2");
            field2.SetupGet(m => m.Name).Returns(new Name("FLD2_NAME"));

            var referenceDataResponseDtos = processor.ProcessMessage(mockmessage.Object);
            Assert.AreEqual(1, referenceDataResponseDtos.Count);
            Assert.AreEqual("AD1 Comdty", referenceDataResponseDtos[0].Ticker);
            Assert.AreEqual("FLD1_NAME", referenceDataResponseDtos[0].Fields[0].Name);
            Assert.AreEqual("Value", referenceDataResponseDtos[0].Fields[0].Value);
            Assert.AreEqual("FLD2_NAME", referenceDataResponseDtos[0].Fields[1].Name);
            Assert.AreEqual("Value2", referenceDataResponseDtos[0].Fields[1].Value);
            Assert.IsTrue(string.IsNullOrEmpty(referenceDataResponseDtos[0].Errors));
        }
    }
}