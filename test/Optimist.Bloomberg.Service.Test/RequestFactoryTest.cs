using System;
using System.Collections.Generic;
using Bloomberglp.Blpapi;
using Moq;
using NUnit.Framework;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomber.Service.Test
{
    public class RequestFactoryTest
    {
        private RequestFactory factory;
        private Mock<Bloomberglp.Blpapi.Service> mockApiService;

        [SetUp]
        public void Setup()
        {
            this.factory = new RequestFactory();
            mockApiService = new Mock<Bloomberglp.Blpapi.Service>(MockBehavior.Strict);
        }
        [Test]
        public void CreateHistorical()
        {
            var start = DateTime.Today.AddDays(-7);
            var end = DateTime.Today;
            var req = new HistoricalRequestDefinition()
            {
                Fields = BlpApiDictionary.DailyPriceFields,
                Start = start,
                End = end,
                Tickers = new List<string>()
                {
                    "AD1 Curncy"
                }
            };
            var mockRequest = new Mock<Request>(MockBehavior.Strict);
            mockApiService.Setup(m => m.CreateRequest(BlpApiDictionary.HistoricalDataRequest.ToString()))
                .Returns(mockRequest.Object);
            var reqSecurities = new List<string>();
            var reqFields = new List<string>();

            var mockSecuritiesElement = new Mock<Element>(MockBehavior.Strict);
            var mockFieldsElement = new Mock<Element>(MockBehavior.Strict);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Securities)).Returns(mockSecuritiesElement.Object);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Fields)).Returns(mockFieldsElement.Object);

            mockSecuritiesElement.Setup(m => m.AppendValue(It.IsAny<string>()))
                .Callback<string>((s) => reqSecurities.Add(s));

            mockFieldsElement.Setup(m => m.AppendValue(It.IsAny<string>()))
                .Callback<string>((s) => reqFields.Add(s));

            mockRequest.Setup(m => m.Set(BlpApiDictionary.StartDate, start.ToString("yyyyMMdd")));
            mockRequest.Setup(m => m.Set(BlpApiDictionary.EndDate, end.ToString("yyyyMMdd")));
            var bdhRequest = factory.CreateHistoricalPriceRequest(mockApiService.Object, req);
            Assert.AreEqual(1,reqSecurities.Count);
            Assert.AreEqual(BlpApiDictionary.DailyPriceFields.Count, reqFields.Count);
        }


        [Test]
        public void CreateReferenceData()
        {
            var flds = new List<Name>() { new Name("FUT_CUR_GEN_TICKER") };
            var req = new ReferenceDataRequestDefinition()
            {
                Fields = flds,
                Tickers = new List<string>()
                {
                    "AD1 Curncy"
                }
            };
            var overrides = new List<FieldValueDto>();
            var mockRequest = new Mock<Request>(MockBehavior.Strict);
            mockApiService.Setup(m => m.CreateRequest(BlpApiDictionary.ReferenceDataRequest.ToString()))
                .Returns(mockRequest.Object);
            var reqSecurities = new List<string>();
            var reqFields = new List<string>();
            var mockSecuritiesElement = new Mock<Element>(MockBehavior.Strict);
            var mockFieldsElement = new Mock<Element>(MockBehavior.Strict);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Securities)).Returns(mockSecuritiesElement.Object);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Fields)).Returns(mockFieldsElement.Object);
            mockSecuritiesElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqSecurities.Add(s));
            mockFieldsElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqFields.Add(s));
            var bdhRequest = factory.CreateReferenceDataRequest(mockApiService.Object, req,overrides);
            Assert.AreEqual(1, reqSecurities.Count);
            Assert.AreEqual(flds.Count, reqFields.Count);
        }

        [Test]
        public void CreateReferenceDataWithOverride()
        {
            var flds = new List<Name>() { new Name("FUT_CUR_GEN_TICKER") };
            var req = new ReferenceDataRequestDefinition()
            {
                Fields = flds,
                Tickers = new List<string>()
                {
                    "AD1 Curncy"
                }
            };
            var overrides = new List<FieldValueDto>() {new FieldValueDto() {Name = "SETTLE_DT",Value = "20170120" } };
            var mockRequest = new Mock<Request>(MockBehavior.Strict);
            mockApiService.Setup(m => m.CreateRequest(BlpApiDictionary.ReferenceDataRequest.ToString()))
                .Returns(mockRequest.Object);
            var reqSecurities = new List<string>();
            var reqFields = new List<string>();
            var mockSecuritiesElement = new Mock<Element>(MockBehavior.Strict);
            var mockFieldsElement = new Mock<Element>(MockBehavior.Strict);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Securities)).Returns(mockSecuritiesElement.Object);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Fields)).Returns(mockFieldsElement.Object);
            mockSecuritiesElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqSecurities.Add(s));
            mockFieldsElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqFields.Add(s));

            var mocOverrides = new Mock<Element>(MockBehavior.Strict);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Overrides)).Returns(mocOverrides.Object);
            var mockOverride = new Mock<Element>(MockBehavior.Strict);
            mocOverrides.Setup(m => m.AppendElement()).Returns(mockOverride.Object);
            mockOverride.Setup(m => m.SetElement(It.IsAny<string>(), It.IsAny<string>()));
            var bdhRequest = factory.CreateReferenceDataRequest(mockApiService.Object, req, overrides);
            Assert.AreEqual(1, reqSecurities.Count);
            Assert.AreEqual(flds.Count, reqFields.Count);
        }

        [Test]
        public void CreateBdhRequest()
        {
            var flds = new List<Name>() { new Name("PX_LAST") };
            var start = new DateTime(2017,1,10);
            var end = new DateTime(2017, 1, 10);
            var req = new HistoricalRequestDefinition()
            {
                Fields = flds,
                Start = start,
                End = end,
                Tickers = new List<string>()
                {
                    "AD1 Curncy"
                }
            };
            var mockRequest = new Mock<Request>(MockBehavior.Strict);
            mockRequest.Setup(m => m.Set(BlpApiDictionary.StartDate, "20170110"));
            mockRequest.Setup(m => m.Set(BlpApiDictionary.EndDate, "20170110"));
            mockApiService.Setup(m => m.CreateRequest(BlpApiDictionary.HistoricalDataRequest.ToString()))
                .Returns(mockRequest.Object);
            var reqSecurities = new List<string>();
            var reqFields = new List<string>();
            var mockSecuritiesElement = new Mock<Element>(MockBehavior.Strict);
            var mockFieldsElement = new Mock<Element>(MockBehavior.Strict);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Securities)).Returns(mockSecuritiesElement.Object);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Fields)).Returns(mockFieldsElement.Object);
            mockSecuritiesElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqSecurities.Add(s));
            mockFieldsElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqFields.Add(s));
            var bdhRequest = factory.CreateBdhRequest(mockApiService.Object, req,new List<FieldValueDto>());
            Assert.AreEqual(1, reqSecurities.Count);
            Assert.AreEqual(flds.Count, reqFields.Count);
            
        }

        [Test]
        public void CreateBdhRequestWithOverride()
        {
            var flds = new List<Name>() { new Name("PX_LAST") };
            var start = new DateTime(2017, 1, 10);
            var end = new DateTime(2017, 1, 10);
            var req = new HistoricalRequestDefinition()
            {
                Fields = flds,
                Start = start,
                End = end,
                Tickers = new List<string>()
                {
                    "AD1 Curncy"
                }
            };
            var mockRequest = new Mock<Request>(MockBehavior.Strict);
            mockRequest.Setup(m => m.Set(BlpApiDictionary.StartDate, "20170110"));
            mockRequest.Setup(m => m.Set(BlpApiDictionary.EndDate, "20170110"));
            mockApiService.Setup(m => m.CreateRequest(BlpApiDictionary.HistoricalDataRequest.ToString()))
                .Returns(mockRequest.Object);
            var reqSecurities = new List<string>();
            var reqFields = new List<string>();
            var mockSecuritiesElement = new Mock<Element>(MockBehavior.Strict);
            var mockFieldsElement = new Mock<Element>(MockBehavior.Strict);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Securities)).Returns(mockSecuritiesElement.Object);
            mockRequest.Setup(m => m.GetElement(BlpApiDictionary.Fields)).Returns(mockFieldsElement.Object);
            mockSecuritiesElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqSecurities.Add(s));
            mockFieldsElement.Setup(m => m.AppendValue(It.IsAny<string>())).Callback<string>((s) => reqFields.Add(s));
            var overr = new FieldValueDto() {Name = "adjustmentFollowDPDF" ,Value = "FALSE"};
            mockRequest.Setup(m => m.Set(overr.Name, overr.Value));
            var bdhRequest = factory.CreateBdhRequest(mockApiService.Object, req,new List<FieldValueDto>() {overr });
            Assert.AreEqual(1, reqSecurities.Count);
            Assert.AreEqual(flds.Count, reqFields.Count);

        }
    }


}