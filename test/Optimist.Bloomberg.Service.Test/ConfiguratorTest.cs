using Autofac;
using NUnit.Framework;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Service.Configurator;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomber.Service.Test
{
    public class ConfiguratorTest
    {
        [Test]
        public void ConfigureServicesTest()
        {
            var builder = new ContainerBuilder();
            Configurator.ConfigureServices(builder);
            var container = builder.Build();
            Assert.IsTrue(container.IsRegistered<IBloombergService>());
        }
    }
}