using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Bloomberglp.Blpapi;
using Moq;
using NUnit.Framework;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Service.Reference;

namespace Optimist.Bloomber.Service.Test
{
    public class ExtensionTest
    {
        [Test]
        public void SchemaTypeDefinitionPrint()
        {
            var mockDefinition = CreateMockTypeDefinition();
            var strElement = mockDefinition.Object.Print();
            Assert.AreEqual("{Name : 'theName', Type: 'STRING' , Description:'this is an schema type' , isComplexType:'False' , isEnumeration:'False', isSimpleType:'True'}", strElement);
        }

        private static Mock<SchemaTypeDefinition> CreateMockTypeDefinition()
        {
            var mockDefinition = new Mock<SchemaTypeDefinition>(MockBehavior.Strict);
            mockDefinition.SetupGet(m => m.Name).Returns(new Name("theName"));
            mockDefinition.SetupGet(m => m.Datatype).Returns(Schema.Datatype.STRING);
            mockDefinition.SetupGet(m => m.Description).Returns("this is an schema type");
            mockDefinition.SetupGet(m => m.IsComplexType).Returns(false);
            mockDefinition.SetupGet(m => m.IsEnumerationType).Returns(false);
            mockDefinition.SetupGet(m => m.IsSimpleType).Returns(true);
            return mockDefinition;
        }


        [Test]
        public void SchemaElementDefinitionPrint()
        {
            var elementDefinition = new Mock<SchemaElementDefinition>(MockBehavior.Strict);
            elementDefinition.SetupGet(m => m.Name).Returns(new Name("theName"));
            elementDefinition.SetupGet(m => m.Description).Returns("this is an schema type");
            var typeDefinition = CreateMockTypeDefinition();
            elementDefinition.SetupGet(m => m.TypeDefinition).Returns(typeDefinition.Object);

            var strElement = elementDefinition.Object.Print();
            Assert.AreEqual("{ Name : 'theName', Description:'this is an schema type', TypeDefinition:  {Name : 'theName', Type: 'STRING' , Description:'this is an schema type' , isComplexType:'False' , isEnumeration:'False', isSimpleType:'True'} }",strElement);
        }


        [Test]
        public void ElementPrint()
        {
            var element = new Mock<Element>(MockBehavior.Strict);

            var schemaElement = new Mock<SchemaElementDefinition>(MockBehavior.Strict);
            schemaElement.SetupGet(m => m.Name).Returns(new Name("The Schema Name"));
            schemaElement.SetupGet(m => m.Description).Returns("this is an schema type");

            var typeDefinition = CreateMockTypeDefinition();
            schemaElement.SetupGet(m => m.TypeDefinition).Returns(typeDefinition.Object);
            element.SetupGet(m => m.NumValues).Returns(2);
            element.SetupGet(m => m.IsComplexType).Returns(true);
            element.SetupGet(m => m.IsArray).Returns(false);
            element.Setup(m => m.GetValue(0)).Returns(10);
            element.Setup(m => m.GetValue(1)).Returns(55);
            element.SetupGet(m => m.NumElements).Returns(0);
            element.SetupGet(m => m.Name).Returns(new Name("The Element Name"));
            element.SetupGet(m => m.Datatype).Returns(Schema.Datatype.STRING);
            element.SetupGet(m => m.ElementDefinition).Returns(schemaElement.Object);
            element.SetupGet(m => m.TypeDefinition).Returns(typeDefinition.Object);
            var strElement = element.Object.Print();
            Assert.IsTrue( strElement.Length > 0);
        }


        [Test]
        public void ToDailyBarDto()
        {
            
            var elementSchema = new Mock<Element>(MockBehavior.Strict);
            elementSchema.Setup(m => m.GetElementAsFloat64(It.IsAny<Name>())).Returns(1);
            elementSchema.SetupGet(m => m.Datatype).Returns(Schema.Datatype.FLOAT64);

            var mockElement = new Mock<Element>(MockBehavior.Strict);
            mockElement.Setup(m => m.GetElementAsDatetime(BlpApiDictionary.Date)).Returns(new Datetime(2016, 1, 1));
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_OPEN)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_HIGH)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_LOW)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_LAST)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_SETTLE)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.VOLUME)).Returns(true);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_OPEN)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_HIGH)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_LOW)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_LAST)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_SETTLE)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.VOLUME)).Returns(elementSchema.Object);
            var expectedOpen = 7;
            var expectedHigh = 10;
            var expectedLow = 5;
            var expectedClose = 6;
            var expectedSettle = 6;
            var expectedVolume = 10000;
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_OPEN)).Returns(expectedOpen);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_HIGH)).Returns(expectedHigh);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_LOW)).Returns(expectedLow);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_LAST)).Returns(expectedClose);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_SETTLE)).Returns(expectedSettle);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.VOLUME)).Returns(expectedVolume);
            var bar = mockElement.Object.ToDailyBarDto("AD1 Curncy");
            Assert.AreEqual(expectedOpen, bar.Open);
            Assert.AreEqual(expectedHigh, bar.High);
            Assert.AreEqual(expectedLow, bar.Low);
            Assert.AreEqual(expectedClose, bar.Close);
            Assert.AreEqual(expectedClose, bar.Settlement);
            Assert.AreEqual(expectedVolume, bar.Volume);
            Assert.AreEqual("AD1 Curncy", bar.Ticker);
        }


        [Test]
        public void ToLastPriceDto()
        {

            var elementSchema = new Mock<Element>(MockBehavior.Strict);
            elementSchema.Setup(m => m.GetElementAsFloat64(It.IsAny<Name>())).Returns(1);
            elementSchema.SetupGet(m => m.Datatype).Returns(Schema.Datatype.FLOAT64);

            var mockElement = new Mock<Element>(MockBehavior.Strict);
            mockElement.Setup(m => m.GetElementAsDatetime(BlpApiDictionary.Date)).Returns(new Datetime(2016, 1, 1));
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_LAST)).Returns(true);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_LAST)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_SETTLE)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_SETTLE)).Returns(true);

            var expectedClose = 6;
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_LAST)).Returns(expectedClose);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_SETTLE)).Returns(expectedClose);

            var bar = mockElement.Object.ToLastPriceDto("AD1 Curncy");
            Assert.AreEqual(expectedClose, bar.PX_LAST);
            Assert.AreEqual("AD1 Curncy", bar.Ticker);
        }



        [Test]
        public void PrintErrorMessage()
        {
            /*
             {Name : 'securityError', DataType : 'SEQUENCE',ElementDefinition: '{ Name : 'securityError', Description:'', TypeDefinition:  {Name : 'ErrorInfo', Type: 'SEQUENCE' , Description:'seqErrorInfo' ,isComplexType:'True' , isEnumeration:'False', isSimpleType:'False'} }',
              Values : [] , 
                 Elements : [
                 {Name : 'source', DataType : 'STRING',ElementDefinition: '{ Name : 'source', Description:'', TypeDefinition:  {Name : 'String', Type: 'STRING' , Description:'' , isComplexType:'False' , isEnumeration:'False', isSimpleType:'True'} }', Values : [110::bbdbd3] , Elements : [] },
                 {Name : 'code', DataType : 'INT32',ElementDefinition: '{ Name : 'code', Description:'', TypeDefinition:  {Name : 'Int32', Type: 'INT32' , Description:'' , isComplexType:'False' , isEnumeration:'False', isSimpleType:'True'} }', Values : [3] , Elements : [] },
                 {Name : 'category', DataType : 'STRING',ElementDefinition: '{ Name : 'category', Description:'', TypeDefinition:  {Name : 'String', Type: 'STRING' , Description:'' , isComplexType:'False' , isEnumeration:'False', isSimpleType:'True'} }', Values : [BAD_SEC] , Elements : [] },
                 {Name : 'message', DataType : 'STRING',ElementDefinition: '{ Name : 'message', Description:'', TypeDefinition:  {Name : 'String', Type: 'STRING' , Description:'' , isComplexType:'False' , isEnumeration:'False', isSimpleType:'True'} }', Values : [Unknown/Invalid Security  [nid:110] ] , Elements : [] },
                 {Name : 'subcategory', DataType : 'STRING',ElementDefinition: '{ Name : 'subcategory', Description:'', TypeDefinition:  {Name : 'String', Type: 'STRING' , Description:'' , isComplexType:'False' , isEnumeration:'False', isSimpleType:'True'} }', Values : [INVALID_SECURITY] , Elements : [] }
                 ] 
                    }
             */
             var mockErrorElement = new Mock<Element>(MockBehavior.Strict);
            mockErrorElement.SetupGet(m => m.Name).Returns(BlpApiDictionary.SecurityError);
            var mockCode = new Mock<Element>(MockBehavior.Strict);
            mockCode.Setup(m => m.GetValueAsString()).Returns("<<code>>");
            var mockCategory = new Mock<Element>(MockBehavior.Strict);
            mockCategory.Setup(m => m.GetValueAsString()).Returns("<<category>>");
            var mockMessage = new Mock<Element>(MockBehavior.Strict);
            mockMessage.Setup(m => m.GetValueAsString()).Returns("<<message>>");
            mockErrorElement.Setup(m => m.GetElement(BlpApiDictionary.Category)).Returns(mockCategory.Object);
            mockErrorElement.Setup(m => m.GetElement(BlpApiDictionary.Code)).Returns(mockCode.Object);
            mockErrorElement.Setup(m => m.GetElement(BlpApiDictionary.Message)).Returns(mockMessage.Object);

            var errorMessage = mockErrorElement.Object.ToSecurityErrorMessage();
            Assert.AreEqual("Category: <<category>> Code: <<code>>  Message: <<message>>", errorMessage);
        }

        [Test]
        public void ToDailyBarDtoWithInvalidPrice()
        {

            var elementSchema = new Mock<Element>(MockBehavior.Strict);
            elementSchema.Setup(m => m.GetElementAsFloat64(It.IsAny<Name>())).Returns(1);
            elementSchema.SetupGet(m => m.Datatype).Returns(Schema.Datatype.FLOAT64);

            var mockElement = new Mock<Element>(MockBehavior.Strict);
            mockElement.Setup(m => m.GetElementAsDatetime(BlpApiDictionary.Date)).Returns(new Datetime(2016, 1, 1));
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_OPEN)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_HIGH)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_LOW)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_LAST)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.PX_SETTLE)).Returns(true);
            mockElement.Setup(m => m.HasElement(BlpApiDictionary.VOLUME)).Returns(true);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_OPEN)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_HIGH)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_LOW)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_LAST)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.PX_SETTLE)).Returns(elementSchema.Object);
            mockElement.Setup(m => m.GetElement(BlpApiDictionary.VOLUME)).Returns(elementSchema.Object);
            var expectedOpen = 0;
            var expectedHigh = 0;
            var expectedLow = 0;
            var expectedClose = 0;
            var expectedSettle = 0;
            var expectedVolume = 10000;
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_OPEN)).Returns(Double.NaN);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_HIGH)).Returns(Double.NaN);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_LOW)).Returns(Double.NaN);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_LAST)).Returns(Double.NaN);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.PX_SETTLE)).Returns(Double.NaN);
            mockElement.Setup(m => m.GetElementAsFloat64(BlpApiDictionary.VOLUME)).Returns(expectedVolume);
            var bar = mockElement.Object.ToDailyBarDto("AD1 Curncy");
            Assert.AreEqual(expectedOpen, bar.Open);
            Assert.AreEqual(expectedHigh, bar.High);
            Assert.AreEqual(expectedLow, bar.Low);
            Assert.AreEqual(expectedClose, bar.Close);
            Assert.AreEqual(expectedClose, bar.Settlement);
            Assert.AreEqual(expectedVolume, bar.Volume);
            Assert.AreEqual("AD1 Curncy", bar.Ticker);
        }
    }
}