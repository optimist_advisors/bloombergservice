using System;
using Bloomberglp.Blpapi;
using Moq;
using NUnit.Framework;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Service.Reference;

namespace Optimist.Bloomber.Service.Test
{
    public class BdhResponseProcessorTest
    {
        [Test]
        public void TestEmpty()
        {
            var processor = new BdhResponseProcessor();
            var mockmessage = new Mock<Message>(MockBehavior.Strict);
            var mockMessageElement = new Mock<Element>(MockBehavior.Strict);
            var mockElementDefinition = new Mock<SchemaElementDefinition>(MockBehavior.Loose);
            var mockSchemaTypeDefinition = new Mock<SchemaTypeDefinition>(MockBehavior.Loose);
            mockElementDefinition.SetupGet(m => m.TypeDefinition).Returns(mockSchemaTypeDefinition.Object);
            mockmessage.Setup(m => m.AsElement).Returns(mockMessageElement.Object);

            mockMessageElement.SetupGet(m => m.NumValues).Returns(0);
            mockMessageElement.SetupGet(m => m.NumElements).Returns(0);
            mockMessageElement.SetupGet(m => m.Name).Returns(BlpApiDictionary.HistoricalDataResponse);
            mockMessageElement.SetupGet(m => m.ElementDefinition).Returns(mockElementDefinition.Object);
            mockMessageElement.SetupGet(m => m.Datatype).Returns(Schema.Datatype.CHOICE);

            var securityElement = new Mock<Element>(MockBehavior.Loose);
            securityElement.Setup(m => m.GetElementAsString(BlpApiDictionary.Security)).Returns("AD1 Comdty");

            mockmessage.Setup(m => m.GetElement(BlpApiDictionary.SecurityData)).Returns(securityElement.Object);

            var fieldDataElement = new Mock<Element>(MockBehavior.Loose);
            securityElement.Setup(m => m.GetElement(BlpApiDictionary.FieldData)).Returns(fieldDataElement.Object);

            var px = processor.ProcessMessage(mockmessage.Object);
            Assert.AreEqual(0, px.Count);
        }

        [Test]
        public void TestOnePrice()
        {
            var processor = new BdhResponseProcessor();
            var mockmessage = new Mock<Message>(MockBehavior.Strict);
            var mockMessageElement = new Mock<Element>(MockBehavior.Strict);
            var mockElementDefinition = new Mock<SchemaElementDefinition>(MockBehavior.Loose);
            var mockSchemaTypeDefinition = new Mock<SchemaTypeDefinition>(MockBehavior.Loose);
            mockElementDefinition.SetupGet(m => m.TypeDefinition).Returns(mockSchemaTypeDefinition.Object);
            mockmessage.Setup(m => m.AsElement).Returns(mockMessageElement.Object);

            mockMessageElement.SetupGet(m => m.NumValues).Returns(0);
            mockMessageElement.SetupGet(m => m.NumElements).Returns(0);
            mockMessageElement.SetupGet(m => m.Name).Returns(BlpApiDictionary.HistoricalDataResponse);
            mockMessageElement.SetupGet(m => m.ElementDefinition).Returns(mockElementDefinition.Object);
            mockMessageElement.SetupGet(m => m.Datatype).Returns(Schema.Datatype.CHOICE);

            var securityElement = new Mock<Element>(MockBehavior.Loose);
            securityElement.Setup(m => m.GetElementAsString(BlpApiDictionary.Security)).Returns("AD1 Comdty");

            mockmessage.Setup(m => m.GetElement(BlpApiDictionary.SecurityData)).Returns(securityElement.Object);

            var fieldDataElement = new Mock<Element>(MockBehavior.Strict);
            fieldDataElement.Setup(m => m.NumValues).Returns(1);
            var mockPriceElement = new Mock<Element>();
            mockPriceElement.Setup(m => m.GetElementAsDatetime(BlpApiDictionary.Date))
                .Returns(new Datetime(DateTime.Today));

            mockPriceElement.Setup(m => m.GetElementAsFloat64(It.IsAny<Name>()))
                .Returns(1.0);
            fieldDataElement.Setup(m => m.GetValueAsElement(0)).Returns(mockPriceElement.Object);
            securityElement.Setup(m => m.GetElement(BlpApiDictionary.FieldData)).Returns(fieldDataElement.Object);

            var px = processor.ProcessMessage(mockmessage.Object);
            Assert.AreEqual(1, px.Count);
        }



    }
}