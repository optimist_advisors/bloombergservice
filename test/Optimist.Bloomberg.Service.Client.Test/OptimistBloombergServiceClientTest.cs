﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomberg.Service.Client.Test
{
    public class OptimistBloombergServiceClientTest
    {
        [Ignore("Doesnt run on teamcity")]
        [Test]
        public void Test()
        {
            var client = new BloombergServiceClient(@"http://192.168.7.151:8888/bloombergservice");
            var px = client.GetHistorical(new List<string>() {"AD1 Curncy"}, DateTime.Today.AddDays(-2), DateTime.Today);
            Assert.IsTrue(px.Count>0);
        }

        [Ignore("Doesnt run on teamcity")]
        [Test]
        public void TestReferenceData()
        {
            var client = new BloombergServiceClient(@"http://192.168.7.151:8888/bloombergserviceuat");
            var px = client.GetReference(new ReferenceDataRequestDto() {Tickers = new List<string>() {"CL1 Comdty"},Fields = new List<string>()
            {
                "PX_SETTLE_ACTUAL_RT",
                "PX_CLOSE",
                "QUOTED_CRNCY",
                "PX_POS_MULT_FACTOR",
                "BPIPE_REFERENCE_SECURITY_CLASS",
                "LAST_UPDATE",
                "LAST_UPDATE_DT",
                "FEED_SOURCE",
                "EQY_PRIM_EXCH",
                "PRICING_SOURCE",
                "PX_SCALING_FACTOR"            } });
            Assert.IsTrue(px.Count > 0);
        }

        [Ignore("Doesnt run on teamcity")]
        [Test]
        public void TestHistoricalData()
        {
            var client = new BloombergServiceClient(@"http://192.168.7.151:8888/bloombergserviceuat");
            var request = new HistoricalDataRequestDto()
            {
                Tickers = new List<string>() {"AAPL Equity"},
                Fields = new List<string>()
                {

                    "PX_LAST",
                    "PX_SETTLE"
                },
                Overrides = new List<FieldValueDto>()
                {
                    new FieldValueDto() {Name = "adjustmentFollowDPDF",Value = "FALSE"} ,
                    new FieldValueDto() {Name = "adjustmentNormal",Value = "FALSE"} ,
                    new FieldValueDto() {Name = "adjustmentAbnormal",Value = "FALSE"} ,
                    new FieldValueDto() {Name = "adjustmentSplit",Value = "FALSE"} , 
                }
                ,From = "20170120"
                ,To = "20170120"
            };
            var px = client.GetHistoricalWithOverrides(request);
            Assert.IsTrue(px.Count > 0);
            Assert.IsTrue(px[0].Fields.Any(f=>f.Name == "PX_LAST"));
            var last = px[0].Fields.FirstOrDefault(s => s.Name == "PX_LAST").Value;
            Assert.AreEqual("120",last);

        }


        [Ignore("Doesnt run on teamcity")]
        [Test]
        public void TestLastHistoricalData()
        {
            var client = new BloombergServiceClient(@"http://192.168.7.151:8888/bloombergserviceuat/");
            var request = new List<string>() { "SPX Index"};
            var px = client.GetHistoricalLast(request,new DateTime(2018,1,1),new DateTime(2018,2,1) );
            Assert.IsTrue(px.Count > 0);

        }
    }
}
