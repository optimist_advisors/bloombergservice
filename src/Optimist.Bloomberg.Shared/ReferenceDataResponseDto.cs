﻿using System.Collections.Generic;

namespace Optimist.Bloomberg.Shared
{
    public class ReferenceDataResponseDto
    {
        public ReferenceDataResponseDto()
        {
            Fields=new List<FieldValueDto>();
        }
        public string Ticker { get; set; }
        public string Errors { get; set; }
        public IList<FieldValueDto> Fields { get; set; }
    }
}