﻿using System;

namespace Optimist.Bloomberg.Shared
{

    public class EMSXExecutionDto
    {
        public String Account { get; set; }
        public double Amount { get; set; }
        public String AssetClass { get; set; }
        public String Broker { get; set; }
        public int BasketId { get; set; }
        public String BBGID { get; set; }
        public String BrokerExecId { get; set; }
        public String BrokerOrderId { get; set; }
        public String ClearingAccount { get; set; }
        public String ClearingFirm { get; set; }
        public DateTime ContractExpDate { get; set; }
        public int CorrectedFillId { get; set; }
        public String Currency { get; set; }
        public String Cusip { get; set; }
        public DateTime DateTimeOfFill { get; set; }
        public String Exchange { get; set; }
        public int ExecPrevSeqNo { get; set; }
        public String ExecType { get; set; }
        public String ExecutingBroker { get; set; }
        public int FillId { get; set; }
        public double FillPrice { get; set; }
        public double FillShares { get; set; }
        public String Isin { get; set; }
        public bool IsLeg { get; set; }
        public String LastCapacity { get; set; }
        public String LastMarket { get; set; }
        public double LimitPrice { get; set; }
        public String Liquidity { get; set; }
        public String LocalExchangeSymbol { get; set; }
        public String MultilegId { get; set; }
        public String OccSymbol { get; set; }
        public String OrderExecutionInstruction { get; set; }
        public String OrderHandlingInstruction { get; set; }
        public int OrderId { get; set; }
        public String OrderInstruction { get; set; }
        public String OrderOrigin { get; set; }
        public String OrderReferenceId { get; set; }
        public double RouteCommissionAmount { get; set; }
        public double RouteCommissionRate { get; set; }
        public String RouteExecutionInstruction { get; set; }
        public String RouteHandlingInstruction { get; set; }
        public int RouteId { get; set; }
        public double RouteNetMoney { get; set; }
        public String RouteNotes { get; set; }
        public double RouteShares { get; set; }
        public String SecurityName { get; set; }
        public String Sedol { get; set; }
        public DateTime? SettlementDate { get; set; }
        public String Side { get; set; }
        public double StopPrice { get; set; }
        public String StrategyType { get; set; }
        public string Ticker { get; set; }
        public String Tif { get; set; }
        public String TraderName { get; set; }
        public int TraderUuid { get; set; }
        public String Type { get; set; }
        public double UserCommissionAmount { get; set; }
        public double UserCommissionRate { get; set; }
        public double UserFees { get; set; }
        public double UserNetMoney { get; set; }
        public String YellowKey { get; set; }
        public string ErrorMsg { get; set; }
        public string BasketName { get; set; }
        public DateTime NyOrderCreateAsOfDateTime { get; set; }
        public DateTime NyTranCreateAsOfDateTime { get; set; }
    }
}