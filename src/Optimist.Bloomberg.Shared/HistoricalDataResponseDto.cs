﻿using System;
using System.Collections.Generic;

namespace Optimist.Bloomberg.Shared
{
    public class HistoricalDataResponseDto
    {
        public HistoricalDataResponseDto()
        {
            Fields=new List<FieldValueDto>();
            
        }
        public string Ticker { get; set; }
        public string Errors { get; set; }
        public DateTime Date { get; set; }
        public IList<FieldValueDto> Fields { get; set; }
    }
}