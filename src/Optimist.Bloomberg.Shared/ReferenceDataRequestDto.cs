﻿using System;
using System.Collections.Generic;

namespace Optimist.Bloomberg.Shared
{
    public class ReferenceDataRequestDto
    {
        public ReferenceDataRequestDto()
        {
            Tickers = new List<string>();
            Fields = new List<string>();
            Overrides = new List<FieldValueDto>();
        }
        public IList<string> Tickers { get; set; }
        public IList<string> Fields{ get; set; }
        public IList<FieldValueDto> Overrides { get; set; }
    }
}