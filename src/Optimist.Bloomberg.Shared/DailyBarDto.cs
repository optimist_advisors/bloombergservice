﻿using System;

namespace Optimist.Bloomberg.Shared
{
    public class DailyBarDto
    {
        public DateTime Date { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public double Settlement { get; set; }
        public double Volume { get; set; }
        
        public string Ticker { get; set; }
        
    }

    public class LastPriceDto
    {
        public DateTime Date { get; set; }
        public double PX_LAST { get; set; }
        public string Ticker { get; set; }
    }
}
