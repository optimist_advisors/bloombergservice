﻿using System;
using System.Collections.Generic;

namespace Optimist.Bloomberg.Shared
{
    public interface IEMSXService
    {
        IList<EMSXExecutionDto> GetHistoricalExecutions(DateTime startDate, DateTime endDate);
    }
}