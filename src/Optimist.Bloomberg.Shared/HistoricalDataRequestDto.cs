﻿using System.Collections.Generic;

namespace Optimist.Bloomberg.Shared
{
    public class HistoricalDataRequestDto
    {
        public IList<string> Tickers { get; set; }
        public IList<string> Fields { get; set; }
        public IList<FieldValueDto> Overrides { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}