﻿using System;
using System.Collections.Generic;

namespace Optimist.Bloomberg.Shared
{
    public interface IBloombergService
    {
        IList<DailyBarDto> GetHistorical(IList<string> symbols, DateTime startDate, DateTime endDate);

        IList<HistoricalDataResponseDto> GetHistoricalWithOverrides(HistoricalDataRequestDto req);

        IList<ReferenceDataResponseDto> GetReference(ReferenceDataRequestDto request);
        IList<LastPriceDto> GetHistoricalLast(IList<string> symbols, DateTime startDate, DateTime endDate);
    }
}