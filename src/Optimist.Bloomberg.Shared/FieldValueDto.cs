namespace Optimist.Bloomberg.Shared
{
    public class FieldValueDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}