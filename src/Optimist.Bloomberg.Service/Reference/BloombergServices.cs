using Bloomberglp.Blpapi;
using log4net;

namespace Optimist.Bloomberg.Service.Reference
{
    public class BloombergApiServices
    {

        public const string ReferenceDataService = @"//blp/refdata";
        public const string MarketDataService = @"//blp/mktdata";
        public const string VWAPService = @"//blp/mktvwap";
        public const string MarketBarSubscriptionService = @"//blp/mktbar";
        public const string APIFieldInformationService = @"//blp/apiflds";
        public const string PageDataService = @"//blp/pagedata";
        public const string TechnicalAnalysisService = @"//blp/tasvc";
        public const string APIAuthorization = @"//blp/apiauth";
        public const string EMSXHistory = @"//blp/emsx.history";
        private static readonly ILog logger = LogManager.GetLogger(typeof(BloombergApiServices));
        public static Bloomberglp.Blpapi.Service GetService(AbstractSession session, string referenceDataService)
        {
            logger.Info($"Opening Service {referenceDataService}");

            if (session.OpenService(referenceDataService))
            {
                logger.Info($"{referenceDataService} Service  Opened");
                return session.GetService(referenceDataService);

            }
            return null;
        }


    }
}