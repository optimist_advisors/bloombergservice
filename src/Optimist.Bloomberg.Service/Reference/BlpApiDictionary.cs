﻿using System.Collections.Generic;
using Bloomberglp.Blpapi;

namespace Optimist.Bloomberg.Service.Reference
{
    public class BlpApiDictionary
    {
        public static readonly Name Fills = new Name("Fills");
        public static readonly Name ServiceOpened = new Name("ServiceOpened");
        public static readonly Name SessionStarted = new Name("SessionStarted");
        public static readonly Name SESSION_STARTUP_FAILURE = new Name("SessionStartupFailure");
        public static readonly Name SERVICE_OPEN_FAILURE = new Name("ServiceOpenFailure");
        public static readonly Name Securities = new Name("securities");
        public static readonly Name Security = new Name("security");
        public static readonly Name SecurityData = new Name("securityData");
        public static readonly Name FieldId = new Name("fieldId");
        public static readonly Name Fields = new Name("fields");
        public static readonly Name FieldData = new Name("fieldData");
        public static readonly Name Date = new Name("date");
        public static readonly Name ResponseError = new Name("responseError");
        public static readonly Name SecurityError = new Name("securityError");
        public static readonly Name FieldExceptions = new Name("fieldExceptions");
        public static readonly Name ErrorInfo = new Name("errorInfo");
        public static readonly Name Category = new Name("category");
        public static readonly Name Message = new Name("message");
        public static readonly Name Code = new Name("code");
        public static readonly Name StartDate = new Name("startDate");
        public static readonly Name EndDate = new Name("endDate");
        public static readonly Name SETTLE_DT = new Name("SETTLE_DT");
        public static readonly Name PX_OPEN = new Name("PX_OPEN"); 
        public static readonly Name PX_HIGH = new Name("PX_HIGH");
        public static readonly Name PX_LOW = new Name("PX_LOW");
        public static readonly Name PX_LAST = new Name("PX_LAST");
        public static readonly Name PX_SETTLE = new Name("PX_SETTLE");
        public static readonly Name VOLUME = new Name("VOLUME");
        public static readonly Name HistoricalDataRequest = new Name("HistoricalDataRequest");
        public static readonly Name HistoricalDataResponse =new Name("HistoricalDataResponse");
        public static readonly Name ReferenceDataRequest = new Name("ReferenceDataRequest");

        public static IList<Name> DailyPriceFields = new List<Name>()
        {
            PX_OPEN,
            PX_HIGH,
            PX_LOW,
            PX_LAST,
            PX_SETTLE,
            VOLUME
        };


        public static IList<Name> LastPriceFields = new List<Name>()
        {
            PX_LAST,
            PX_SETTLE,
        };
        internal static Name FillsResponseType = new Name("GetFillsResponse");
        public static readonly Name ReferenceDataResponse = new Name("ReferenceDataResponse");
        public static readonly Name Overrides = new Name("overrides");
        public static readonly Name  Value  = new Name("value");
    }
}
