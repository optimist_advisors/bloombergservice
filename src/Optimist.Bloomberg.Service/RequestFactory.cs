using System;
using System.Collections.Generic;
using System.Linq;
using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{

    public interface IRequestFactory
    {
        Request CreateHistoricalPriceRequest(Bloomberglp.Blpapi.Service apiService, HistoricalRequestDefinition req);

        Request CreateReferenceDataRequest(Bloomberglp.Blpapi.Service apiService, ReferenceDataRequestDefinition req, IList<FieldValueDto> overrides);
        Request CreateBdhRequest(Bloomberglp.Blpapi.Service service, HistoricalRequestDefinition reqParams, IList<FieldValueDto> overrides);

    }

    public class RequestFactory : IRequestFactory
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(RequestFactory));
        public const string DayFormat = "yyyyMMdd";
        public Request CreateHistoricalPriceRequest(Bloomberglp.Blpapi.Service apiService, HistoricalRequestDefinition req)
        {
            var request = apiService.CreateRequest(BlpApiDictionary.HistoricalDataRequest.ToString());
            // Add securities to request
            WriteSecurities(request, req.Tickers);
            WriteFields(request, req.Fields);
            WriteDateRange(request, req.Start, req.End);
            return request;

        }

        public Request CreateReferenceDataRequest(Bloomberglp.Blpapi.Service apiService, ReferenceDataRequestDefinition req,IList<FieldValueDto> overrides)
        {
            var request = apiService.CreateRequest(BlpApiDictionary.ReferenceDataRequest.ToString());
            // Add securities to request
            WriteSecurities(request, req.Tickers);
            WriteFields(request, req.Fields);

            
            foreach (var overrideField in overrides)
            {
                AddOverride(overrideField, request);
            }

            return request;
        }

        private static void AddOverride(FieldValueDto overrideField, Request request)
        {
            var overrides = request.GetElement(BlpApiDictionary.Overrides);
            logger.Info("Getting OverrideElement");
            var element = overrides.AppendElement();
            logger.Info($"Setting Override FieldId : {overrideField.Name} ");
            element.SetElement(BlpApiDictionary.FieldId.ToString(),overrideField.Name);
            logger.Info($"Setting Override value : {overrideField.Value} ");
            element.SetElement(BlpApiDictionary.Value.ToString(), overrideField.Value);
        }

        public Request CreateBdhRequest(Bloomberglp.Blpapi.Service apiService, HistoricalRequestDefinition req,IList<FieldValueDto> overrides)
        {
            logger.InfoFormat("Creating Bdh Request");
            var request = apiService.CreateRequest(BlpApiDictionary.HistoricalDataRequest.ToString());
            // Add securities to request
            WriteSecurities(request, req.Tickers);
            WriteFields(request, req.Fields);
            foreach (var overrideField in overrides)
            {
                request.Set(overrideField.Name,overrideField.Value.ToString() );
            }
            WriteDateRange(request, req.Start, req.End);

            return request;
        }

        private void WriteDateRange(Request request, System.DateTime startDate, System.DateTime endDate)
        {
            logger.DebugFormatExt("Adding DateRange to request : [{0},{1}]", startDate.ToString(DayFormat), endDate.ToString(DayFormat));

            request.Set(BlpApiDictionary.StartDate, startDate.ToString(DayFormat));
            request.Set(BlpApiDictionary.EndDate, endDate.ToString(DayFormat));
        }

        private void WriteFields(Request request, IList<Name> fields)
        {
            logger.DebugFormatExt("Adding fields to request : [{0}]", fields.Select(s=>s.ToString()).ToJson(false));
            var requestFields = request.GetElement(BlpApiDictionary.Fields);
            foreach (var field in fields)
            {
                requestFields.AppendValue(field.ToString());
            }
        }

        private void WriteSecurities(Request request, IList<string> securities)
        {
            logger.DebugFormatExt("Adding securities to request : [{0}]", securities.ToJson(false));

            var requestSecurities = request.GetElement(BlpApiDictionary.Securities);
            foreach (var ticker in securities)
            {
                requestSecurities.AppendValue(ticker);
            }
        }


    }




}