﻿/* Copyright 2017. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:  The above
 * copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

using System;
using System.Linq;
using Bloomberglp.Blpapi;
using Element = Bloomberglp.Blpapi.Element;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomberg.Service
{
    public static class EMSHistoryExtension
    {
        public static void ReadFromElement(this EMSXExecutionDto data,Element fill )
        {
            try
            {
                data.Account = fill.GetElementAsString("Account");
                data.Amount = fill.GetElementAsFloat64("Amount");
                data.AssetClass = fill.GetElementAsString("AssetClass");
                data.BasketId = fill.GetElementAsInt32("BasketId");
                data.BasketName = fill.GetElementAsString("BasketName");
                data.BBGID = fill.GetElementAsString("BBGID");
                data.Broker = fill.GetElementAsString("Broker");
                data.BrokerExecId = fill.GetElementAsString("BrokerExecId");
                data.BrokerOrderId = fill.GetElementAsString("BrokerOrderId");
                data.ClearingAccount = fill.GetElementAsString("ClearingAccount");
                data.ClearingFirm = fill.GetElementAsString("ClearingFirm");
                data.ContractExpDate = fill.GetElementAsDate("ContractExpDate").ToSystemDateTime();
                data.CorrectedFillId = fill.GetElementAsInt32("CorrectedFillId");
                data.Currency = fill.GetElementAsString("Currency");
                data.Cusip = fill.GetElementAsString("Cusip");
                data.DateTimeOfFill = fill.GetElementAsDate("DateTimeOfFill").ToSystemDateTime();
                data.Exchange = fill.GetElementAsString("Exchange");
                data.ExecPrevSeqNo = fill.GetElementAsInt32("ExecPrevSeqNo");
                data.ExecType = fill.GetElementAsString("ExecType");
                data.ExecutingBroker = fill.GetElementAsString("ExecutingBroker");
                data.FillId = fill.GetElementAsInt32("FillId");
                data.FillPrice = fill.GetElementAsFloat64("FillPrice");
                data.FillShares = fill.GetElementAsFloat64("FillShares");
                data.Isin = fill.GetElementAsString("Isin");
                data.IsLeg = fill.GetElementAsBool("IsLeg");
                data.LastCapacity = fill.GetElementAsString("LastCapacity");
                data.LastMarket = fill.GetElementAsString("LastMarket");
                data.LimitPrice = fill.GetElementAsFloat64("LimitPrice");
                data.Liquidity = fill.GetElementAsString("Liquidity");
                data.LocalExchangeSymbol = fill.GetElementAsString("LocalExchangeSymbol");
                data.MultilegId = fill.GetElementAsString("MultilegId");
                data.OccSymbol = fill.GetElementAsString("OCCSymbol");
                data.OrderExecutionInstruction = fill.GetElementAsString("OrderExecutionInstruction");
                data.OrderHandlingInstruction = fill.GetElementAsString("OrderHandlingInstruction");
                data.OrderId = fill.GetElementAsInt32("OrderId");
                data.OrderInstruction = fill.GetElementAsString("OrderInstruction");
                data.OrderOrigin = fill.GetElementAsString("OrderOrigin");
                data.OrderReferenceId = fill.GetElementAsString("OrderReferenceId");
                data.RouteCommissionAmount = fill.GetElementAsFloat64("RouteCommissionAmount");
                data.RouteCommissionRate = fill.GetElementAsFloat64("RouteCommissionRate");
                data.RouteExecutionInstruction = fill.GetElementAsString("RouteExecutionInstruction");
                data.RouteHandlingInstruction = fill.GetElementAsString("RouteHandlingInstruction");
                data.RouteId = fill.GetElementAsInt32("RouteId");
                data.RouteNetMoney = fill.GetElementAsFloat64("RouteNetMoney");
                data.RouteNotes = fill.GetElementAsString("RouteNotes");
                data.RouteShares = fill.GetElementAsFloat64("RouteShares");
                data.SecurityName = fill.GetElementAsString("SecurityName");
                data.Sedol = fill.GetElementAsString("Sedol");
                data.Side = fill.GetElementAsString("Side");
                data.StopPrice = fill.GetElementAsFloat64("StopPrice");
                data.StrategyType = fill.GetElementAsString("StrategyType");
                data.Ticker = fill.GetElementAsString("Ticker");
                data.Tif = fill.GetElementAsString("TIF");
                data.TraderName = fill.GetElementAsString("TraderName");
                data.TraderUuid = fill.GetElementAsInt32("TraderUuid");
                data.Type = fill.GetElementAsString("Type");
                data.UserCommissionAmount = fill.GetElementAsFloat64("UserCommissionAmount");
                data.UserCommissionRate = fill.GetElementAsFloat64("UserCommissionRate");
                data.UserFees = fill.GetElementAsFloat64("UserFees");
                data.UserNetMoney = fill.GetElementAsFloat64("UserNetMoney");
                data.YellowKey = fill.GetElementAsString("YellowKey");

                if (fill.HasElement("NyOrderCreateAsOfDateTime"))
                {
                    data.NyOrderCreateAsOfDateTime = fill.GetElementAsDatetime("NyOrderCreateAsOfDateTime").ToSystemDateTime();
                }
                if (fill.HasElement("NyTranCreateAsOfDateTime"))
                {
                    data.NyTranCreateAsOfDateTime = fill.GetElementAsDatetime("NyTranCreateAsOfDateTime").ToSystemDateTime();
                }
                if (fill.HasElement("SettlementDate"))
                {
                    data.SettlementDate = fill.GetElementAsDatetime("SettlementDate").ToSystemDateTime();
                }

            }
            catch (Exception ex)
            {
                data.ErrorMsg = $"{ex.Message}  {fill.ToString()} ";
            }
        }

    }
}
