﻿using Autofac;
using log4net;
using log4net.Config;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomberg.Service.Configurator
{
    public class Configurator
    {
        public static void ConfigureServices(ContainerBuilder builder)
        {
            builder.Register(x => new RequestFactory()).As<IRequestFactory>().SingleInstance();
            builder.Register(x => new SessionFactory()).As<ISessionFactory>().SingleInstance();
            builder.Register(x => new BloombergService(x.Resolve<IRequestFactory>(),x.Resolve<ISessionFactory>())).As<IBloombergService>().SingleInstance();
            builder.Register(x => new BloombergEMSXService(x.Resolve<IRequestFactory>(), x.Resolve<ISessionFactory>())).As<IEMSXService>().SingleInstance();

        }

        public static void ConfigureLog()
        {
            if (!LogManager.GetRepository().Configured)
            {
                XmlConfigurator.Configure();
            }
        }
    }
}
