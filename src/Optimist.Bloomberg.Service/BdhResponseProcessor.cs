using System.Collections.Generic;
using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{
    public class BdhResponseProcessor : ResponseProcessor<HistoricalDataResponseDto>
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(BdhResponseProcessor));

        public override IList<HistoricalDataResponseDto> ProcessMessage(Message msg)
        {
            var securityDefinition = msg.GetElement(BlpApiDictionary.SecurityData);
            var ticker = securityDefinition.GetElementAsString(BlpApiDictionary.Security);
            var priceDataElement = securityDefinition.GetElement(BlpApiDictionary.FieldData);
            var bars = new List<HistoricalDataResponseDto>();
            if (priceDataElement.NumValues <= 0)
                return bars;

            for (int j = 0; j < priceDataElement.NumValues; j++)
            {
                
                var element = priceDataElement.GetValueAsElement(j);
                var currentBar = element.ToHistoricalDataResponseDto(ticker);
                logger.InfoFormat($"Read Bar {currentBar.ToJson(false)}");

                bars.Add(currentBar);
            } //end of for

            return bars;
        }
    }
}