﻿using System;
using Bloomberglp.Blpapi;
using log4net;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{
    public interface ISessionFactory
    {
        Session Create();
        void RunOnSession(Action<Session> sessionHandler);
        void RunOnAsyncSession(Action<Session> sessionHandler);

    }
    public class SessionFactory : ISessionFactory
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SessionFactory));
        private SessionOptions options;

        public SessionFactory()
        {
            options = new SessionOptions();
            options.ServerHost = "localhost";
            options.ServerPort = 8194;
            
        }
        public Session Create()
        {
            return new Session(options);
        }

        public void RunOnSession(Action<Session> sessionHandler)
        {
            using (var session = Create())
            {
                try
                {
                    logger.DebugFormat("Starting Bloomberg session");
                    if (session.Start())
                    {
                        sessionHandler.Invoke(session);
                    }
                    else
                    {
                        var error = String.Format("Cant Start Bloomberg Session with Params : {0}", options.ToJson());
                        throw new Exception(error);
                    }
                    session.Stop(AbstractSession.StopOption.SYNC);
                }
                catch (Exception ex)
                {
                    logger.Fatal("Error processing Request against Bloomberg API", ex);
                    throw ex;
                }
            }
        }

        public void RunOnAsyncSession(Action<Session> sessionHandler)
        {
            using (var session = Create())
            {
                try
                {
                    logger.DebugFormat("Starting Bloomberg session");
                    session.StartAsync();
                    sessionHandler.Invoke(session);
                    
                    session.Stop(AbstractSession.StopOption.SYNC);
                }
                catch (Exception ex)
                {
                    logger.Fatal("Error processing Request against Bloomberg API", ex);
                    throw ex;
                }
            }
        }
    }
}