using System;
using System.Collections.Generic;
using System.Linq;
using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{
    public class ReferenceDataResponseProcessor : ResponseProcessor<ReferenceDataResponseDto>
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ReferenceDataResponseProcessor));

        public override IList<ReferenceDataResponseDto> ProcessMessage(Message msg)
        {
             var result = new List<ReferenceDataResponseDto>();
            logger.DebugFormatExt("Trying to get ReferenceData from message : {0}", msg.AsElement.Print());
            var securities = msg.GetElement(BlpApiDictionary.SecurityData);
            var nSecurities = securities.NumValues;
            logger.InfoFormat($"Processing  {nSecurities} Securities");
            for (var iSecurity = 0; iSecurity < nSecurities; iSecurity++)
            {
                var securityElement = securities.GetValueAsElement(iSecurity);
                if (securityElement.HasElement(BlpApiDictionary.SecurityError))
                {
                    var ticker = securityElement.GetElementAsString(BlpApiDictionary.Security);
                    logger.Debug(securityElement.Print());
                    string error = securityElement.GetElement(BlpApiDictionary.SecurityError).ToSecurityErrorMessage();
                    logger.WarnFormatExt("Errors Found For Ticker {0} {1}", ticker,error);
                    result.Add(new ReferenceDataResponseDto()
                    {
                        Errors = error,
                        Ticker = ticker
                    });
                    continue;
                }
                if (securityElement.HasElement(BlpApiDictionary.FieldData))
                {
                    var securityResponse = ReadSecurityResponse(securityElement);
                    result.Add(securityResponse);
                }
            }


            return result;
        }

        private static ReferenceDataResponseDto ReadSecurityResponse(Element security)
        {
            var ticker = security.GetElementAsString(BlpApiDictionary.Security);
            logger.InfoFormat($"Processing  {ticker} ");
            var fields = security.GetElement(BlpApiDictionary.FieldData);
            var securityResponse = new ReferenceDataResponseDto()
            {
                Ticker = ticker,
            };
            for (int i = 0; i < fields.NumElements; i++)
            {
                var fldResp = fields.GetElement(i);
                var fieldDto = new FieldValueDto()
                {
                    Name = fldResp.Name.ToString(),
                    Value = fldResp.GetValueAsString()
                };
                securityResponse.Fields.Add(fieldDto);
            }
            return securityResponse;
        }
    }
}