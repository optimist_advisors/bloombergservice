﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{
    public class BloombergService : IBloombergService
    {
        private readonly IRequestFactory requestFactory;
        private readonly ISessionFactory sessionFactory;
        private static readonly ILog logger = LogManager.GetLogger(typeof(BloombergService));

        public BloombergService(IRequestFactory requestFactory, ISessionFactory sessionFactory)
        {
            this.requestFactory = requestFactory;
            this.sessionFactory = sessionFactory;
        }

        public IList<DailyBarDto> GetHistorical(IList<string> symbols, DateTime startDate, DateTime endDate)
        {
            var result = new List<DailyBarDto>();
            var reqParams = new HistoricalRequestDefinition()
            {
                Fields = BlpApiDictionary.DailyPriceFields,
                Start = startDate,
                End = endDate,
                Tickers = symbols
            };
            sessionFactory.RunOnSession(session =>
            {
                var service = BloombergApiServices.GetService(session, BloombergApiServices.ReferenceDataService);
                var request = this.requestFactory.CreateHistoricalPriceRequest(service, reqParams);
                var response = session.SendRequestAndWaitResponse(request);
                var processor = new HistoricBarsResponseProcessor();
                var r = processor.ProcessResponse(response);
                if (r.Any())
                    result.AddRange(r);
            });
            return result;
        }

        public IList<HistoricalDataResponseDto> GetHistoricalWithOverrides(HistoricalDataRequestDto req)
        {
            var result = new List<HistoricalDataResponseDto>();
            var reqParams = new HistoricalRequestDefinition()
            {
                Fields = BlpApiDictionary.DailyPriceFields,
                Start = req.From.FromUrl(),
                End = req.To.FromUrl(),
                Tickers = req.Tickers
            };
            sessionFactory.RunOnSession(session =>
            {
                var service = BloombergApiServices.GetService(session, BloombergApiServices.ReferenceDataService);
                var request = this.requestFactory.CreateBdhRequest(service, reqParams, req.Overrides);
                var response = session.SendRequestAndWaitResponse(request);
                var processor = new BdhResponseProcessor();
                var r = processor.ProcessResponse(response);
                if (r.Any())
                    result.AddRange(r);
            });
            return result;
        }

        public IList<ReferenceDataResponseDto> GetReference(ReferenceDataRequestDto request)
        {
            return this.GetReferenceData(request.Tickers, request.Fields, request.Overrides);
        }

        public IList<LastPriceDto> GetHistoricalLast(IList<string> symbols, DateTime startDate, DateTime endDate)
        {
            var result = new List<LastPriceDto>();
            var overrides = new List<FieldValueDto>()
            {
                new FieldValueDto() {Name = "adjustmentFollowDPDF", Value = "TRUE"},
                new FieldValueDto() {Name = "adjustmentNormal", Value = "FALSE"},
                new FieldValueDto() {Name = "adjustmentAbnormal", Value = "FALSE"},
                new FieldValueDto() {Name = "adjustmentSplit", Value = "FALSE"},
            };
            var reqParams = new HistoricalRequestDefinition()
            {
                Fields = BlpApiDictionary.LastPriceFields,
                Start = startDate,
                End = endDate,
                Tickers = symbols
            };
            sessionFactory.RunOnSession(session =>
            {
                var service = BloombergApiServices.GetService(session, BloombergApiServices.ReferenceDataService);
                var request = this.requestFactory.CreateBdhRequest(service, reqParams, overrides);
                var response = session.SendRequestAndWaitResponse(request);
                var processor = new LastPriceResponseProcessor();
                var r = processor.ProcessResponse(response);
                if (r.Any())
                    result.AddRange(r);
            });
            return result;
        }

        public IList<ReferenceDataResponseDto> GetReferenceData(IList<string> symbols, IList<string> fields,
            IList<FieldValueDto> overrides)
        {
            logger.Info("Get Reference Data");
            logger.InfoFormatExt($"Getting Fieds {fields.ToJson(false)} for {symbols.ToJson(false)}");
            var result = new List<ReferenceDataResponseDto>();
            var reqParams = new ReferenceDataRequestDefinition()
            {
                Fields = new List<Name>(),
                Tickers = symbols
            };
            foreach (var fld in fields)
            {
                reqParams.Fields.Add(new Name(fld));
            }

            sessionFactory.RunOnSession(session =>
            {
                var service = BloombergApiServices.GetService(session, BloombergApiServices.ReferenceDataService);
                var request = this.requestFactory.CreateReferenceDataRequest(service, reqParams, overrides);
                logger.InfoFormatExt($"SENDING REQUEST {request.ToJson(false)}");
                var response = session.SendRequestAndWaitResponse(request);
                var processor = new ReferenceDataResponseProcessor();
                var r = processor.ProcessResponse(response);
                if (r.Any())
                    result.AddRange(r);
            });
            return result;
        }
    }
}