using System;
using System.Collections.Generic;
using Bloomberglp.Blpapi;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomberg.Service
{
    public class HistoricalRequestDefinition : RequestDefinition
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }


    }


    public class ReferenceDataRequestDefinition : RequestDefinition { }
    public class RequestDefinition
    {
        public IList<string> Tickers { get; set; } 
        public IList<Name> Fields { get; set; } 

    }
}