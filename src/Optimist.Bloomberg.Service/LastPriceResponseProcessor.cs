using System.Collections.Generic;
using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomberg.Service
{
    public class LastPriceResponseProcessor : ResponseProcessor<LastPriceDto>
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(HistoricBarsResponseProcessor));

        public override IList<LastPriceDto> ProcessMessage(Message msg)
        {
            logger.DebugFormatExt("Trying to get prices from message : {0}", msg.AsElement.Print());
            var securityDefinition = msg.GetElement(BlpApiDictionary.SecurityData);
            var ticker = securityDefinition.GetElementAsString(BlpApiDictionary.Security);
            var priceDataElement = securityDefinition.GetElement(BlpApiDictionary.FieldData);
            var bars = new List<LastPriceDto>();
            if (priceDataElement.NumValues <= 0)
                return bars;

            for (int j = 0; j < priceDataElement.NumValues; j++)
            {
                var element = priceDataElement.GetValueAsElement(j);
                var currentBar = element.ToLastPriceDto(ticker);
                bars.Add(currentBar);
            } //end of for

            return bars;
        }
    }
}