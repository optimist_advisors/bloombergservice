﻿using System.Collections.Generic;
using Bloomberglp.Blpapi;
using log4net;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{
    public class EMSXExecutionResponseProcessor : ResponseProcessor<EMSXExecutionDto>
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(BdhResponseProcessor));

        public override IList<EMSXExecutionDto> ProcessMessage(Message msg)
        {
            logger.Info($"Processing {msg.ToString()}");
            var result = new List<EMSXExecutionDto>();
            if (msg.MessageType.Equals(BlpApiDictionary.FillsResponseType))
            {
                Element fills = msg.GetElement(BlpApiDictionary.Fills);
                int numValues = fills.NumValues;

                for (int i = 0; i < numValues; i++)
                {

                    Element fill = fills.GetValueAsElement(i);
                    var dto = new EMSXExecutionDto();
                    dto.ReadFromElement(fill);
                    result.Add(dto);
                }
            }
            return result;
        }
    }
}