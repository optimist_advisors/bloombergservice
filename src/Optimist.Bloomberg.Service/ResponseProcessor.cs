using System.Collections.Generic;
using System.Linq;
using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;

namespace Optimist.Bloomberg.Service
{
    public abstract class ResponseProcessor<T>
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ResponseProcessor<T>));

        public IList<T> ProcessResponse(IList<Event> events)
        {
            var result = new List<T>();
            foreach (var e in events)
            {
                var messages = e.GetMessages();
                foreach (var m in messages)
                {
                    logger.InfoFormatExt(@"Processing {0} message:\n {1}", m.MessageType, m.Print());
                    if (m.HasElement(BlpApiDictionary.ResponseError))
                    {
                        logger.WarnFormatExt(@"Skipping {0}",m.Print());
                        continue;
                    }
                    var prices = ProcessMessage(m);
                    if (prices.Any())
                        result.AddRange(prices);
                }
            }
            return result;
        }

        public abstract IList<T> ProcessMessage(Message message);
    }
}