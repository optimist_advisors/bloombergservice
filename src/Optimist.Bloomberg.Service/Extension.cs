using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using log4net;
using log4net.Util;
using Bbg = Bloomberglp.Blpapi;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{
    public static class MessageExtension
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(MessageExtension));


        public static LastPriceDto ToLastPriceDto(this Bbg.Element dataElement, string ticker)
        {
            var currentBar = new LastPriceDto();
            currentBar.Ticker = ticker;
            currentBar.Date = dataElement.ReadDateField(BlpApiDictionary.Date);
            currentBar.PX_LAST= dataElement.ReadDoubleField(BlpApiDictionary.PX_LAST, 0);

            logger.DebugFormatExt("ToDailyBarDto {0}", currentBar.ToJson(false));
            return currentBar;
        }


        public static DailyBarDto ToDailyBarDto(this Bbg.Element dataElement,string ticker)
        {
            var currentBar = new DailyBarDto();
            currentBar.Ticker = ticker;
            currentBar.Date = dataElement.ReadDateField(BlpApiDictionary.Date);
            currentBar.Open = dataElement.ReadDoubleField( BlpApiDictionary.PX_OPEN, 0);
            currentBar.High = dataElement.ReadDoubleField(BlpApiDictionary.PX_HIGH, 0);
            currentBar.Low = dataElement.ReadDoubleField(BlpApiDictionary.PX_LOW, 0);
            currentBar.Close = dataElement.ReadDoubleField(BlpApiDictionary.PX_LAST, 0);
            currentBar.Settlement = dataElement.ReadDoubleField(BlpApiDictionary.PX_SETTLE, 0);
            currentBar.Volume = dataElement.ReadDoubleField(BlpApiDictionary.VOLUME, 0);
            logger.DebugFormatExt("ToDailyBarDto {0}",currentBar.ToJson(false));
            return currentBar;
    }

        public static string StreamToString(this Stream stream)
        {
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public static System.DateTime ReadDateField(this Bbg.Element dataElement, Bbg.Name name)
        {
            var date = dataElement.GetElementAsDatetime(name);
            System.DateTime sDate = new System.DateTime(date.Year, date.Month, date.DayOfMonth);
            return sDate;

        }

        public static double ReadDoubleField(this Bbg.Element element, Bbg.Name fieldName, double defaultValue)
        {
            if (!element.HasElement(fieldName))
                return defaultValue;
            
            var dValue = element.GetElementAsFloat64(fieldName);
            if (Double.IsNaN(dValue) )
                return defaultValue;
            return dValue;
        }

        public static string Print(this Bbg.Message message)
        {
            var str = String.Format(@" TopicName: '{0}',MessageType : '{1}' , Elements : {2} ", 
                message.TopicName,
                message.MessageType.ToString(), 
                message.Elements.Print());             
            return str;
        }


        public static string Print(this IEnumerable<Bbg.Element> elements)
        {
            var list = elements.ToList();
            if (!list.Any())
                return "[]";

            return "[" + String.Join(",", list.Select(s=>s.Print())) + "]";
        }

        public static string Print(this Bbg.SchemaTypeDefinition schema)
        {
            var str = String.Format("{{Name : '{0}', Type: '{1}' , Description:'{2}' , isComplexType:'{3}' , isEnumeration:'{4}', isSimpleType:'{5}'}}",
                schema.Name,
                schema.Datatype.ToString(),
                schema.Description,
                schema.IsComplexType,
                schema.IsEnumerationType,
                schema.IsSimpleType);            
            return str;

        }

        public static string Print(this Bbg.SchemaElementDefinition schema)
        {
            var str = String.Format(@"{{ Name : '{0}', Description:'{1}', TypeDefinition:  {2} }}",
                schema.Name,
                schema.Description,
                schema.TypeDefinition.Print());
            return str;
        }

        public static string ToSecurityErrorMessage(this Bbg.Element element)
        {
            if (element.Name.ToString() != BlpApiDictionary.SecurityError.ToString())
                return null;

            var message = element.GetElement(BlpApiDictionary.Message);
            var category = element.GetElement(BlpApiDictionary.Category);
            var code = element.GetElement(BlpApiDictionary.Code);
            return $"Category: {category.GetValueAsString()} Code: {code.GetValueAsString()}  Message: {message.GetValueAsString()}";
        }

        public static HistoricalDataResponseDto ToHistoricalDataResponseDto(this Bbg.Element bdhElement, string ticker)
        {
            var currentBar = new HistoricalDataResponseDto();
            currentBar.Ticker = ticker;
            currentBar.Date = ReadDateField(bdhElement, BlpApiDictionary.Date);

            for (int i = 0; i < bdhElement.NumElements; i++)
            {
                var element = bdhElement.GetElement(i);
                if (element.Name == BlpApiDictionary.Date)
                    continue;

                var field = new FieldValueDto() {Name = element.Name.ToString()};
                currentBar.Fields.Add(new FieldValueDto()
                {
                    Name = element.Name.ToString(),Value = element.ToDtoValue().ToString()
                });

            }
            logger.DebugFormatExt("ToDailyBarDto {0}", currentBar.ToJson(false));
            return currentBar;
        }

        public static string ToDtoValue(this Bbg.Element element)
        {
            switch (element.Datatype)
            {
                case Bbg.Schema.Datatype.DATE:
                    var dt  = element.GetValueAsDate();
                    return dt.ToSystemDateTime().ToString(CultureInfo.InvariantCulture);
                case Bbg.Schema.Datatype.FLOAT32:
                    return element.GetValueAsFloat32().ToString(CultureInfo.InvariantCulture);
                case Bbg.Schema.Datatype.FLOAT64:
                    //todo : try to setup machine to dont use , as decimal separator
                    return element.GetValueAsFloat64().ToString(CultureInfo.InvariantCulture); 
                    ;
                default:
                    return element.GetValueAsString();
                    break;
            }
;
        }

        public static string Print(this Bbg.Element element)
        {
            var strValues = ValuesToString(element);
            var strElements = element.Elements.ToList().Print();
            var root = String.Format(@"{{Name : '{0}', DataType : '{1}',ElementDefinition: '{2}', Values : {3} , Elements : {4} }}",
                element.Name,
                element.Datatype.ToString(),
                element.ElementDefinition.Print(),
                strValues,
                strElements);
            return root;
        }

        public static string ValuesToString(Bbg.Element element)
        {
            var strValues = "[";
            for (int i = 0; i < element.NumValues; i++)
            {
                if (element.IsComplexType || element.IsArray)
                    continue;
                var o = element.GetValue(i);
                strValues += o.ToString();
                if (i < element.NumValues - 1)
                    strValues += ",";
            }
            strValues += "]";
            return strValues;
        }



        public static IList<Bbg.Event> SendRequestAndWaitResponse(this Bbg.Session session, Bbg.Request request)
        {
            
            return SendRequestAndWaitResponse(session,request, new Bbg.CorrelationID(Guid.NewGuid()));
        }

        public static IList<Bbg.Event> SendRequestAndWaitResponse(this Bbg.Session session, Bbg.Request request, Bbg.CorrelationID reqId)
        {
            session.SendRequest(request, reqId);
            var responseEvents = new List<Bbg.Event>();
            var processing = true;
            while (processing)
            {
                var nextEvent = session.NextEvent();
                logger.InfoExt($"{nextEvent.Type.ToString()} Event Received");

                switch (nextEvent.Type)
                {
                    case Bbg.Event.EventType.PARTIAL_RESPONSE:
                        responseEvents.Add(nextEvent);
                        break;
                    case Bbg.Event.EventType.RESPONSE:
                        responseEvents.Add(nextEvent);
                        processing = false;
                        break;
                    
                    case Bbg.Event.EventType.SERVICE_STATUS:
                    case Bbg.Event.EventType.SESSION_STATUS:
                    default:
                        foreach (var m in nextEvent.GetMessages())
                        {
                            if (m.MessageType.Equals(new Bbg.Name("RequestFailure")) )
                            {
                                processing = false;
                                try
                                {
                                    var errorInfo = m.Elements.FirstOrDefault();
                                    logger.Info($"{errorInfo.Name} : {errorInfo.Datatype}");
                                    if (errorInfo != null)
                                    {
                                        var tt = "-";
                                        foreach (var errorItem in errorInfo.Elements)
                                        {
                                            if (!errorItem.IsComplexType)
                                            {
                                                logger.Info($"{tt} {errorItem.ElementDefinition.Name} : {errorItem.GetValueAsString()}");
                                            }
                                            else
                                            {
                                                logger.Info($"{tt} {errorItem.ElementDefinition.Name} : << Complex >>");

                                            }
                                        }
                                    }
                                    logger.Error($"Childs: {m.NumElements} FragmentType: {m.FragmentType} ");
                                    var childElement = m.Elements.FirstOrDefault();
                                    logger.Info($"{childElement.ElementDefinition.Print()}");
//                                    logger.Error($"Child As String: {childElement.GetValueAsString()}");
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex);
                                }
                            }
                            else
                            {
                                logger.WarnFormatExt("Message Type: {0}", m.MessageType);
                                var stream = new MemoryStream();
                                m.Print(stream);
                                logger.WarnFormatExt("Content: {0}", stream.StreamToString());

                            }
                        }

                        break;
                }
            }

            return responseEvents;
        }
    }
}