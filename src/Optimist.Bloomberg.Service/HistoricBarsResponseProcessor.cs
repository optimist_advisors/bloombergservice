using System.Collections.Generic;
using System.Linq;
using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomberg.Service
{
    public class HistoricBarsResponseProcessor : ResponseProcessor<DailyBarDto>
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(HistoricBarsResponseProcessor));

        public override IList<DailyBarDto> ProcessMessage(Message msg)
        {
            logger.DebugFormatExt("Trying to get prices from message : {0}", msg.AsElement.Print());
            var securityDefinition = msg.GetElement(BlpApiDictionary.SecurityData);
            var ticker = securityDefinition.GetElementAsString(BlpApiDictionary.Security);
            var priceDataElement = securityDefinition.GetElement(BlpApiDictionary.FieldData);
            var bars = new List<DailyBarDto>();
            if (priceDataElement.NumValues <= 0)
                return bars;

            for (int j = 0; j < priceDataElement.NumValues; j++)
            {
                var element = priceDataElement.GetValueAsElement(j);
                var currentBar = element.ToDailyBarDto(ticker);
                bars.Add(currentBar);
            } //end of for

            return bars;
        }
    }
}