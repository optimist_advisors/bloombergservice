﻿using Bloomberglp.Blpapi;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service.Reference;
using Optimist.Bloomberg.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Optimist.Common.Model.Extensions;

namespace Optimist.Bloomberg.Service
{
    public class BloombergEMSXService : IEMSXService
    {
        private readonly IRequestFactory requestFactory;
        private readonly ISessionFactory sessionFactory;
        private static readonly ILog logger = LogManager.GetLogger(typeof(BloombergService));

        public BloombergEMSXService(IRequestFactory requestFactory, ISessionFactory sessionFactory)
        {
            this.requestFactory = requestFactory;
            this.sessionFactory = sessionFactory;
        }

        public IList<EMSXExecutionDto> GetHistoricalExecutions(DateTime startDate, DateTime endDate)
        {
            var result = new List<EMSXExecutionDto>();
            sessionFactory.RunOnAsyncSession(session =>
            {
                WaitSessionInit(session);
                var serviceName = BloombergApiServices.EMSXHistory;
                logger.Info($"Opening {serviceName}");
                if (session.OpenService(serviceName))
                {

                    if (!WaitServiceToOpen(session))
                        throw new Exception("Timeout Waiting Service");

                    logger.Info($"{serviceName} Service  Opened");
                    var service = session.GetService(BloombergApiServices.EMSXHistory);
                    logger.Info("Creating Request");
                    var request = this.CreateGetFills(service, startDate, endDate);
                    logger.Info($"Sending Request {request.ToJson(false)}");
                    var response = session.SendRequestAndWaitResponse(request, new CorrelationID());
                    var processor = new EMSXExecutionResponseProcessor();
                    var r = processor.ProcessResponse(response);
                    if (r.Any())
                        result.AddRange(r);

                }
            });
            return result;
        }


        private static bool WaitSessionInit(Session session)
        {
            var waitingSessionReady = true;
            var result = false;
            var count = 10;
            logger.Info("Waiting Session");
            while (waitingSessionReady && count > 0)
            {
                var nextEvent = session.NextEvent();
                switch (nextEvent.Type)
                {
                    case Bloomberglp.Blpapi.Event.EventType.SESSION_STATUS:
                    {
                        foreach (var m in nextEvent.GetMessages())
                        {
                            logger.WarnFormatExt("Message Type: {0}", m.MessageType);
                            if (m.MessageType.Equals(BlpApiDictionary.SessionStarted))
                            {
                                waitingSessionReady = false;
                                result = true;
                            }

                            if (m.MessageType.Equals(BlpApiDictionary.SESSION_STARTUP_FAILURE))
                            {
                                logger.Error("SOMETHING FAILED STARTING SESSION");
                                return false;

                            }
                            
                        }
                    }
                        break;
                    default:
                        PrintEvent(nextEvent);
                        break;
                }

                count--;
                Task.Delay(TimeSpan.FromMilliseconds(100)).Wait();

            }

            return result;
        }

        private static void PrintEvent(Event nextEvent)
        {
            foreach (var m in nextEvent.GetMessages())
            {
                logger.WarnFormatExt("Message Type: {0}", m.MessageType);
                var stream = new MemoryStream();
                m.Print(stream);
                logger.WarnFormatExt("Content: {0}", stream.StreamToString());
            }
        }

        private static bool WaitServiceToOpen(Session session)
        {
            var waitingService = true;
            var result = false;
            var count = 10;
            logger.Info("Waiting Service to open");
            while (waitingService && count > 0)
            {
                var nextEvent = session.NextEvent();


                switch (nextEvent.Type)
                {
                    case Event.EventType.SERVICE_STATUS:
                        {
                            foreach (var m in nextEvent.GetMessages())
                            {
                                logger.WarnFormatExt("Message Type: {0}", m.MessageType);
                                if (m.MessageType.Equals(BlpApiDictionary.ServiceOpened))
                                {
                                    waitingService = false;
                                    result = true;
                                }

                                var stream = new MemoryStream();
                                m.Print(stream);
                                logger.WarnFormatExt("Content: {0}", stream.StreamToString());
                            }
                        }
                        break;
                    case Bloomberglp.Blpapi.Event.EventType.SESSION_STATUS:
                    default:
                        foreach (var m in nextEvent.GetMessages())
                        {
                            logger.WarnFormatExt("Message Type: {0}", m.MessageType);
                            var stream = new MemoryStream();
                            m.Print(stream);
                            logger.WarnFormatExt("Content: {0}", stream.StreamToString());
                        }

                        break;
                }

                count--;
                Task.Delay(TimeSpan.FromMilliseconds(100)).Wait();

            }

            return result;
        }

        public Request CreateGetFills(Bloomberglp.Blpapi.Service service, DateTime from, DateTime to)
        {
            logger.Info($"Creating Request Fill from {from} to {to}");

            Request request = service.CreateRequest("GetFills");

            // The Date/Time values from and to may contain a timezone element
            request.Set("FromDateTime", $"{from:yyyy-MM-ddT00:00:00.000+00:00}");
            request.Set("ToDateTime", $"{to:yyyy-MM-ddT23:00:00.000+00:00}");

            var scope = request.GetElement("Scope");



            // One or more specified UUIDs
            scope.SetChoice("Uuids");
            scope.GetElement("Uuids").AppendValue(8674493);


            return request;
        }

    }
}