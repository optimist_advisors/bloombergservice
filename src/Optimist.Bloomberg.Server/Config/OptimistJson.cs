﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Optimist.Bloomberg.Server.Config
{
    public class OptimistJsonMediaTypeFormatter : JsonMediaTypeFormatter
    {
        public OptimistJsonMediaTypeFormatter(CultureInfo cultureInfo)
        {
            this.SerializerSettings = new OptimistJsonSerializerSettings(cultureInfo);
        }
    }

    public class OptimistJsonSerializerSettings : JsonSerializerSettings
    {
        public OptimistJsonSerializerSettings(CultureInfo cultureInfo)
        {
            this.Culture = cultureInfo;
            this.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }


}
