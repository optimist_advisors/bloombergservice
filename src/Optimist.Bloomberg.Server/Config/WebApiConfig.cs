﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using Autofac;
using Autofac.Integration.WebApi;
using log4net;
using Optimist.Bloomberg.Server.Controllers;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Service.Configurator;
using Optimist.Bloomberg.Shared;

namespace Optimist.Bloomberg.Server.Config
{
    public static class WebApiConfig
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(WebApiConfig));

        static readonly CultureInfo CurrentUiCulture = new CultureInfo("en-US");

        public static void Configure(HttpConfiguration config)
        {
            Configure(config, CreateBuilder(config));
        }

        public static void Configure(HttpConfiguration config, ContainerBuilder builder, bool runStatableInstances = true)
        {
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            Log.Info("Configure an ExceptionLogger");
            config.Services.Add(typeof(IExceptionLogger), new Log4NetExceptionLogger());

            Log.Info("Comienza a configurar el servidor.");
            Log.Info($"Using Culture {CurrentUiCulture}");
            Thread.CurrentThread.CurrentUICulture = CurrentUiCulture;
            Thread.CurrentThread.CurrentCulture = CurrentUiCulture;
            config.Formatters.Clear();
            config.Formatters.Add(new OptimistJsonMediaTypeFormatter(CurrentUiCulture));

            Log.Info("Construye el container");
            var container = builder.Build();

            if (runStatableInstances)
            {
                Log.Info("Start all the startable instances.");
                var startables = container.Resolve<IEnumerable<IStartable>>();
                foreach (var startable in startables)
                {
                    startable.Start();
                }
            }

            Log.Info("Crea un dependency resolver");
            var resolver = new AutofacWebApiDependencyResolver(container);

            Log.Info("Configura Web API con el dependecy resolver");
            config.DependencyResolver = resolver;

            GlobalConfiguration.Configuration.DependencyResolver = resolver;
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
        }

        public static ContainerBuilder CreateBuilder(HttpConfiguration config)
        {
            Log.Info("Crea el container");
            var builder = new ContainerBuilder();

            Configurator.ConfigureServices(builder);

            Log.Info("Registra los Web API controllers");
            builder.Register(x => new BloombergController(x.Resolve<IBloombergService>(),x.Resolve<IEMSXService>())).InstancePerRequest();
            return builder;
        }

        public static void RegisterRoutes(HttpConfiguration config)
        {
            Log.Info("Registra las rutas");
            config.MapHttpAttributeRoutes();
        }
    }

    public class Log4NetExceptionLogger : ExceptionLogger
    {
        public const string LogErrorName = "App_Error";
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HttpApplication));

        public override void Log(ExceptionLoggerContext context)
        {
            Logger.Error(LogErrorName, context.Exception);
            base.Log(context);
        }
    }
}
