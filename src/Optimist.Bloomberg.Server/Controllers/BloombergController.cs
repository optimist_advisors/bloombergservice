﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using log4net;
using log4net.Util;
using Optimist.Bloomberg.Service;
using Optimist.Bloomberg.Shared;
using Optimist.Common.Model.Extensions;


namespace Optimist.Bloomberg.Server.Controllers
{
    public class BloombergController : ApiController
    {
        private readonly IBloombergService bloombergService;
        private readonly IEMSXService eMSXService;
        private static readonly ILog logger = LogManager.GetLogger(typeof(BloombergController));

        public BloombergController(IBloombergService bloombergService,IEMSXService eMSXService)
        {
            this.bloombergService = bloombergService;
            this.eMSXService = eMSXService;
        }


        [HttpPost, Route("api/bdh/{from}/{to}")]
        public IList<DailyBarDto> GetHistoricalPrices(string from, string to, IList<string> symbols)
        {
            DateTime startDate  = from.FromUrl();
            DateTime endDate = to.FromUrl();
            return bloombergService.GetHistorical(symbols,startDate,endDate);
        }

        [HttpPost, Route("api/bdh/{from}/{to}/last")]
        public IList<LastPriceDto> GetHistoricalLastPrices(string from, string to, IList<string> symbols)
        {
            DateTime startDate = from.FromUrl();
            DateTime endDate = to.FromUrl();
            return bloombergService.GetHistoricalLast(symbols, startDate, endDate);
        }

        [HttpGet, Route("api/emsx/executions/{from}/{to}")]
        public IList<EMSXExecutionDto> GetHistoricalLastPrices(string from, string to)
        {
            DateTime startDate = from.FromUrl();
            DateTime endDate = to.FromUrl();
            return eMSXService.GetHistoricalExecutions( startDate, endDate);
        }


        [HttpPost, Route("api/bdp")]
        public IList<ReferenceDataResponseDto> GetReferenceData([FromBody]ReferenceDataRequestDto dto)
        {
            return bloombergService.GetReference(dto);
        }

        [HttpPost, Route("api/bdh")]
        public IList<HistoricalDataResponseDto> GetHistoricalData([FromBody]HistoricalDataRequestDto dto)
        {
            logger.InfoFormatExt($"Get Historical Data {dto.ToJson(false)}");
            return bloombergService.GetHistoricalWithOverrides(dto);
        }
    }
}
