﻿using System;
using System.Collections.Generic;
using Optimist.Bloomberg.Shared;
using Optimist.Common.ServiceClient;
using Optimist.Common.Model.Extensions;
namespace Optimist.Bloomberg.Service.Client
{
    public class BloombergServiceClient : BaseServiceClient , IBloombergService
    {
        public BloombergServiceClient(string url ) : base(new RestClientFactory(url))
        {
        }

        public IList<DailyBarDto> GetHistorical(IList<string> symbols, DateTime startDate, DateTime endDate)
        {
            var url = String.Format("api/bdh/{0}/{1}",startDate.ToUrl(),endDate.ToUrl()); ;
            return Post<IList<DailyBarDto>, IList<string>>(url, symbols);
        }

        public IList<HistoricalDataResponseDto> GetHistoricalWithOverrides(HistoricalDataRequestDto req)
        {
            var url = String.Format("api/bdh"); ;
            return Post<IList<HistoricalDataResponseDto>, HistoricalDataRequestDto>(url, req);
        }

        public IList<ReferenceDataResponseDto> GetReference(ReferenceDataRequestDto request)
        {
            var url = "api/bdp/"; ;
            return Post<IList<ReferenceDataResponseDto>, ReferenceDataRequestDto>(url, request);
        }

        public IList<LastPriceDto> GetHistoricalLast(IList<string> symbols, DateTime startDate, DateTime endDate)
        {
            var url = String.Format("api/bdh/{0}/{1}/last", startDate.ToUrl(), endDate.ToUrl()); ;
            return Post<IList<LastPriceDto>, IList<string>>(url, symbols);
        }
    }
}
