﻿using System;
using System.Collections.Generic;
using Optimist.Bloomberg.Shared;
using Optimist.Common.ServiceClient;
using Optimist.Common.Model.Extensions;
namespace Optimist.Bloomberg.Service.Client
{
    public class EMSXServiceClient : BaseServiceClient, IEMSXService
    {
        public EMSXServiceClient(string url) : base(new RestClientFactory(url))
        {
        }

        public IList<EMSXExecutionDto> GetHistoricalExecutions(DateTime startDate, DateTime endDate)
        {
            var url = String.Format("api/emsx/executions/{0}/{1}", startDate.ToUrl(), endDate.ToUrl()); ;
            return Get<IList<EMSXExecutionDto>>(url);
        }
    }
}
